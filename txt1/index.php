<?php
/**
 * 这是一个最简单的TXT留言板
 *
 * @author ShuangYa
 * @link https://git.oschina.net/phpbar/Message-Board
 * @license https://git.oschina.net/phpbar/Message-Board/blob/master/LICENSE
 */
@error_reporting(E_ALL &~ E_NOTICE);
header('Content-type: text/html; charset=utf-8');
?>
<html>
<head>
	<title>留言板</title>
	<link href="http://cdn.bootcss.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="container">
<?php
//读取留言数据
$content = @trim(file_get_contents('messages.txt'));
if (!$content) {
	echo '没有留言';
} else {
	//分割留言
	$content = explode("\n", $content);
	//遍历
	foreach ($content as $k => $v) {
		list($author, $time, $body) = explode("'", $v);
		//输出
	?>
	<div class="panel panel-default">
		<div class="panel-heading"><b><?=$author?></b> <?=date('Y-m-d H:i:s', $time)?></div>
		<div class="panel-body"><?=$body?></div>
	</div>
	<?php
	}
}
//输出留言框
?>
	<div class="panel panel-default">
		<div class="panel-heading">我要留言</div>
		<div class="panel-body">
			<form action="submit.php" method="post">
				<p><label>您的名字：<input type="text" name="name" class="form-control" style="width:auto;display:inline-block;"></label></p>
				<p>内容：</p>
				<textarea name="body" class="form-control"></textarea>
				<p><input type="submit" value="留言" class="btn btn-primary"></p>
			</form>
		</div>
	</div>
</div>
</body>
</html>