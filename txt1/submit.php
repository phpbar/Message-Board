<?php
/**
 * 这是一个最简单的TXT留言板
 *
 * @author ShuangYa
 * @link https://git.oschina.net/phpbar/Message-Board
 * @license https://git.oschina.net/phpbar/Message-Board/blob/master/LICENSE
 */
@error_reporting(E_ALL &~ E_NOTICE);
header('Content-type: text/html; charset=utf-8');
if (empty($_POST['name'])) {
	echo '姓名不能为空';
	exit;
}
if (empty($_POST['body'])) {
	echo '内容不能为空';
	exit;
}
//过滤掉HTML实体
$name = htmlspecialchars($_POST['name']);
$body = htmlspecialchars($_POST['body']);
//将空行替换成换行标签
$body = str_replace("\n", '<br>', $body);
//写入文件
$writeStr = $name . "'" . time() . "'" . $body . "\n";
$fp = fopen('messages.txt', 'a');
if (!$fp) {
	echo '文件打开失败';
	exit;
}
fwrite($fp, $writeStr);
@fclose($fp);
echo '留言成功';
