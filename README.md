# Message Board

这里是一些用于学习的留言板小程序

# 目录

### TXT留言板

[点我查看](txt1)

最简单的留言板之一

### SQL留言板

[点我查看](sql1)

也是最简单的留言板之一

# 开源许可

所有代码均遵循GPLv2协议开源

# 使用注意

您可以随意使用、分发此代码，但未经书面允许，禁止用于商业用途，包括但不限于培训等
