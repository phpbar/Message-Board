<?php
/**
 * 这是一个最简单的TXT留言板
 *
 * @author ShuangYa
 * @link https://git.oschina.net/phpbar/Message-Board
 * @license https://git.oschina.net/phpbar/Message-Board/blob/master/LICENSE
 */
require('common.php');
if (empty($_POST['name'])) {
	echo '姓名不能为空';
	exit;
}
if (empty($_POST['body'])) {
	echo '内容不能为空';
	exit;
}
//过滤掉HTML实体
$name = htmlspecialchars($_POST['name']);
$body = htmlspecialchars($_POST['body']);
//将空行替换成换行标签
$body = str_replace("\n", '<br>', $body);
sql_connect();
message_add($name, date('Y-m-d H:i:s'), $body);
echo '留言成功';
