<?php

/**
 * 这是一个简单的留言板
 *
 * @author ShuangYa
 * @link https://git.oschina.net/phpbar/Message-Board
 * @license https://git.oschina.net/phpbar/Message-Board/blob/master/LICENSE
 */
@error_reporting(E_ALL &~ E_NOTICE);
date_default_timezone_set('Asia/Shanghai');
header('Content-type: text/html; charset=utf-8');

//配置
$db = [
	'host' => 'localhost',
	'port' => 3306,
	'user' => 'root',
	'password' => 'root',
	'dbname' => 'message'
];

$dbconn = NULL; //数据库连接
//定义一些函数
function sql_connect() {
	global $db, $dbconn;
	$dbconn = mysqli_connect($db['host'], $db['user'], $db['password'], $db['dbname'], $db['port']);
	if (!$dbconn) {
		echo 'Connect Error (', mysqli_connect_errno(), ') ', mysqli_connect_error();
		exit;
	}
}
function sql_query($sql) {
	global $dbconn;
	return mysqli_query($dbconn, $sql);
}
function sql_fetch_array($result) {
	return mysqli_fetch_array($result , MYSQLI_ASSOC);
}

function message_add($name, $time, $body) {
	$sql = 'INSERT INTO `messages`(`name`, `time`, `body`) VALUES (' . "'$name', '$time', '$body')";
	if (!sql_query($sql)) {
		return FALSE;
	}
	return TRUE;
}